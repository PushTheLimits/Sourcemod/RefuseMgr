/* 
###################################################################################
# Copyright © 2017 Kevin Urbainczyk <kevin@rays3t.info> - All Rights Reserved     #
# Unauthorized copying of this file, via any medium is strictly prohibited.       #
# Proprietary and confidential.                                                   #
#                                                                                 #
# This file is part of the RefuseMgr-Plugin.                                      #
# Written by Kevin 'RAYs3T' Urbainczyk <kevin@rays3t.info>                        #
# Homepage: https://ptl-clan.de                                                   #
###################################################################################
*/

// Includes
#include <autoexecconfig>
#include <morecolors>
#include <cstrike>

// Code style rules
#pragma semicolon 1
#pragma newdecls required

#define PREFIX "{white}[{aqua}Refuse{white}]{default}"

static int g_refuseCounter[MAXPLAYERS + 1] =  { 0, ... };

static ConVar g_refuseAmount;
static ConVar g_refuseTime;

static Handle g_refuseTimer[MAXPLAYERS + 1] =  { INVALID_HANDLE, ... };

public Plugin myinfo =
{
	name = "refusemgr",
	author = "Kevin 'RAYs3T' Urbainczyk",
	description = "Let players refuse games",
	version = "${-version-}",
	url = "https://ptl-clan.de"
};


public void OnPluginStart()
{
	RegPluginLibrary("refusemgr");
	CreateNative("RefuseAddAmount", Native_RefuseAddAmount);
	
	AutoExecConfig_SetFile("refusemgr");
	g_refuseAmount		= AutoExecConfig_CreateConVar("refusemgr_refuse_amount", "1", "Amount of refuses a player have");
	g_refuseTime 		= AutoExecConfig_CreateConVar("refusemgr_refuse_time", "180.0", "Time in seconds the refusing player is painted");
	
	AutoExecConfig_ExecuteFile();
	AutoExecConfig_CleanFile();
	
	HookEvent("round_start", Event_RoundStart, EventHookMode_Post);
	
	RegConsoleCmd("v", Command_PlayerRefuse, "Let the player refuse a game");
	RegConsoleCmd("clear", Command_Clear, "Removes all colors from palyers");
	
	InitializeRefuseCounter();
	
	LoadTranslations("refusemgr.phrases");
}

public Action Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	InitializeRefuseCounter();
}

public Action Command_PlayerRefuse(int client, int args)
{
	if(!IsPlayerAlive(client))
	{
		CPrintToChat(client, "%t", "not_alive");
		return Plugin_Handled;
	}
	
	// May reset already running reset timer
	if(g_refuseTimer[client] != INVALID_HANDLE)
	{
		ResetRefuse(client);
		KillTimer(g_refuseTimer[client]);
		g_refuseTimer[client] = INVALID_HANDLE;
	}
	
	if(g_refuseCounter[client] > 0)
	{
		Refuse(client);
		g_refuseCounter[client]--;
	}
	else
	{
		CPrintToChat(client, "%s %t", PREFIX, "no_refuses_left");
	}
	
	return Plugin_Handled;
}

public Action Command_Clear(int client, int args)
{
	int invokerTeam = GetClientTeam(client);
	if(invokerTeam != CS_TEAM_CT)
	{
		CPrintToChat(client, "%s %t", PREFIX, "clear_invalid_team");
		return Plugin_Handled;
	}
	
	for (int i = 0; i < MaxClients; i++)
	{
		if(g_refuseTimer[i] != INVALID_HANDLE)
		{
			ResetRefuse(i);
			KillTimer(g_refuseTimer[i]);
			g_refuseTimer[i] = INVALID_HANDLE;
		}
	}
	char invokerName[64];
	GetClientName(client, invokerName, sizeof(invokerName));
	CPrintToChatAll("%s %t", PREFIX, "players_cleared", invokerName);
	return Plugin_Handled;
}

static void InitializeRefuseCounter()
{
	int amount = GetConVarInt(g_refuseAmount);
	for (int i = 0; i < MaxClients; i++)
	{
		g_refuseCounter[i] = amount;
		if(g_refuseTimer[i] != INVALID_HANDLE)
		{
			KillTimer(g_refuseTimer[i]);
			g_refuseTimer[i] = INVALID_HANDLE;
		}
	}
}

static void Refuse(int client)
{
	if(!IsValidClient(client))
	{
		return;
	}
	SetEntityRenderMode(client,RENDER_TRANSCOLOR);
	SetEntityRenderColor(client, 0, 0, 255, 255);
	
	float refuseTime = GetConVarFloat(g_refuseTime);
	g_refuseTimer[client] = CreateTimer(refuseTime, Timer_ResetRefuse, client);
	
	char refuserName[64];
	GetClientName(client, refuserName, sizeof(refuserName));
	
	CPrintToChatAll("%s %t", PREFIX, "player_refused", refuserName);
}

public Action Timer_ResetRefuse(Handle timer, int client)
{
	ResetRefuse(client);
	g_refuseTimer[client] = INVALID_HANDLE;
	return Plugin_Stop;
}

static void ResetRefuse(int client)
{
	if(!IsValidClient(client))
	{
		return;
	}
	SetEntityRenderMode(client,RENDER_TRANSCOLOR);
	SetEntityRenderColor(client, 255, 255, 255, 255);
}

public int Native_RefuseAddAmount(Handle plugin, int params)
{
	int client = GetNativeCell(1);
	int additionalAmount = GetNativeCell(2);
	
	if(!IsValidClient(client))
	{
		return;
	}
	g_refuseCounter[client] += additionalAmount;
}

stock bool IsValidClient(int client, bool nobots = true)
{ 
    if (client <= 0 || client > MaxClients || !IsClientConnected(client) || (nobots && IsFakeClient(client)))
    {
        return false; 
    }
    return IsClientInGame(client); 
}